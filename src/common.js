import React from 'react';
import { AutocompleteInput } from 'react-admin';

export const CustomAutocompleteInput = ({ choices, source, optionText, label }) => {
    return (
        <AutocompleteInput source={source} choices={choices} optionText={optionText} label={label} />
    )
}