import React from 'react';
import {
    List, Datagrid, Edit, Create, SimpleForm,
    TextField, DateField,
    ReferenceField, TextInput, BooleanField, BooleanInput, ReferenceInput
} from 'react-admin';
import { CustomAutocompleteInput } from '../common';
import SellerGroup from '@material-ui/icons/SupervisorAccount';
import { required } from 'ra-core';
export const SellerIcon = SellerGroup;

export const SellerList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="fullName" label="Full Name" />
            <BooleanField source="isApproved" />
            <ReferenceField source="user.id" reference="User" label="User's email">
                <TextField source="email" />
            </ReferenceField>
            <DateField label="Register time (MM/DD/YYYY)" source="createdAt" />
        </Datagrid>
    </List>
);

const ElementInputs = () => {
    return (
        <div>
            <div>
                <TextInput source="firstName" label="First Name" />
            </div>
            <TextInput source="lastName" label="Last Name" />
            <div>
                <TextInput disabled source="avatarUrl" label="Avatar Url" />
            </div>
        </div>
    );
};

const SellerTitle = ({ record }) => {
    return <span>Seller {record ? `"${record.id}"` : ''}</span>;
};

export const SellerEdit = (props) => (
    <Edit title={<SellerTitle />} {...props}>
        <SimpleForm>
            <BooleanInput source="isApproved" label="Is Approved"/>
            <TextInput disabled source="id" label="ID" />
            {/* <TextInput disabled style={{ minWidth: '250px' }} source="email" type="email" label="Email" validate={required()} /> */}
            <ElementInputs />
            <DateField label="Register time (MM/DD/YYYY)" source="createdAt" showTime />
        </SimpleForm>
    </Edit>
);

export const SellerCreate = (props) => (
    <Create title="Create a Seller" {...props}>
        <SimpleForm>
            <ReferenceInput source="user.id" reference="User" label="User's email">
                <CustomAutocompleteInput optionText="email" />
            </ReferenceInput>
            {/* <ElementInputs /> */}
        </SimpleForm>
    </Create>
);
