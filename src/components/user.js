import React from 'react';
import {
    List, Datagrid, Edit, Create, SimpleForm,
    TextField, DateField,
    RadioButtonGroupInput, DateInput, TextInput, BooleanInput, NumberInput, AutocompleteInput, ReferenceInput
} from 'react-admin';
import UserGroup from '@material-ui/icons/SupervisorAccount';
import { required } from 'ra-core';
export const UserIcon = UserGroup;

export const UserList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="email" label="Email" />
            <TextField source="role" label="Role" />
            <TextField source="displayName" label="Display Name" />
            <DateField label="Register time (MM/DD/YYYY)" source="createdAt" />
        </Datagrid>
    </List>
);

const ElementInputs = () => {
    return (
        <div>
            <div>
                <TextInput source="displayName" label="Display Name" />
            </div>
        </div>
    );
};

const UserTitle = ({ record }) => {
    return <span>User {record ? `"${record.id}"` : ''}</span>;
};

export const UserEdit = (props) => (
    <Edit title={<UserTitle />} {...props}>
        <SimpleForm>
            <TextInput disabled source="id" label="ID" />
            <TextInput disabled style={{ minWidth: '250px' }} source="email" type="email" label="Email" validate={required()} />
            <ElementInputs />
            <DateField label="Register time (MM/DD/YYYY)" source="createdAt" showTime />
        </SimpleForm>
    </Edit>
);

export const UserCreate = (props) => (
    <Create title="Create a User" {...props}>
        <SimpleForm>
            <TextInput style={{ minWidth: '250px' }} source="email" type="email" label="Email" validate={required()} />
            <ElementInputs />
            <TextInput source="password" label="Password" />
        </SimpleForm>
    </Create>
);
